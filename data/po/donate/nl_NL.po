# 
# Translators:
# Gerard Aalders <wizd3m@habbekrats.org>, 2016
# Robbie Deighton <deightonrobbie@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-04-03 14:00+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Dutch (Netherlands) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/nl_NL/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nl_NL\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: donate.html:17
msgid "Donate"
msgstr "Doneer"

#: donate.html:25
msgid "About Donations"
msgstr "Over donaties."

#: donate.html:26
msgid "Our Supporters"
msgstr "Onze supporters"

#: donate.html:29
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "Het lijkt erop dat je geen verbinding hebt met internet. Controleer je verbinding om toegang te krijgen."

#: donate.html:30
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "Excuses, Welkom kon geen verbinding maken."

#: donate.html:31
msgid "Retry"
msgstr "Opnieuw proberen"

#: donate.html:40
msgid ""
"Ubuntu MATE is funded by our community. Your donations help pay towards:"
msgstr "Ubuntu MATE wordt gesteund door onze gemeenschap. Jouw donaties helpen bij het betalen van:"

#: donate.html:42
msgid "Web hosting and bandwidth costs."
msgstr "Web hosting en bandbreedte kosten."

#: donate.html:43
msgid "Supporting Open Source projects that Ubuntu MATE depends upon."
msgstr "Ondersteunen van Open Source projecten waar Ubuntu MATE afhankelijk van is."

#: donate.html:44
msgid ""
"Eventually pay for full time developers of Ubuntu MATE and MATE Desktop."
msgstr "Uiteindelijk het betalen van full time ontwikkelaars van Ubuntu MATE en de MATE Desktop."

#: donate.html:46
msgid "Patreon"
msgstr "Patreon"

#: donate.html:46
msgid ""
"is a unique way to fund an Open Source project. A regular monthly income "
"ensures sustainability for the Ubuntu MATE project. Patrons will be rewarded"
" with exclusive project news, updates and invited to participate in video "
"conferences where you can talk to the developers directly."
msgstr "is een unieke manier om een Open Source project te ondersteunen. Een maandelijks inkomen garandeert de duurzaamheid van het Ubuntu MATE project. Doneurs krijgen exclusief project nieuws, updates en worden uitgenodigd om deel te nemen aan video conferenties waar ze kunnen praten met de ontwikkelaars."

#: donate.html:51
msgid "If you would prefer to use"
msgstr "Als je een voorkeur hebt voor"

#: donate.html:51
msgid ""
"then we have options to donate monthly or make a one off donation. Finally, "
"we also accept donations via"
msgstr "dan zijn er mogelijkheden om maandelijks te doneren of om een eenmalige donatie te doen. En als laatste, we accepteren ook donaties via"

#: donate.html:53
msgid "Bitcoin"
msgstr "Bitcoin"

#: donate.html58, 71
msgid "Become a Patron"
msgstr "Wordt een Patron"

#: donate.html:61
msgid "Donate with PayPal"
msgstr "Doneer met PayPal"

#: donate.html:64
msgid "Donate with Bitcoin"
msgstr "Doneer met Bitcoin"

#: donate.html:70
msgid "Commercial sponsorship"
msgstr "Commerciële sponsoring"

#: donate.html:70
msgid "is also available. Click the"
msgstr ""

#: donate.html:71
msgid "button above to find out more."
msgstr "knop voor meer informatie."

#: donate.html:79
msgid "Thank you!"
msgstr "Bedankt!"

#: donate.html:80
msgid ""
"Every month, we compile a list of our donations and detail how they are "
"spent."
msgstr "Elke maand maken we een lijst van de donaties en hoe we deze uitgeven."

#: donate.html:81
msgid "This information is published monthly to the"
msgstr "Deze informatie wordt elke maand gepubliceerd op de"

#: donate.html:81
msgid "Ubuntu MATE Blog"
msgstr "Ubuntu MATE Blog"

#: donate.html:82
msgid "which you can take a look below."
msgstr "welke je hier beneden kunt bekijken."

#: donate.html:88
msgid "Year"
msgstr "Jaar"

#: donate.html:89
msgid "Month"
msgstr "Maand"
